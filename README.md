# Java Easing Functions

Simple classes representing different easing functions which can be used to calculate the response (Y) to an input (X) by applying the function. 
Also has a simple Swing GUI to plot and test functions with different parameters. 