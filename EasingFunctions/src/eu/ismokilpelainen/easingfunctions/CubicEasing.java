package eu.ismokilpelainen.easingfunctions;

/**
 * Class representing cubic easing function of form y=a*x<sup>3</sup>.
 * @author Ismo Kilpeläinen
 *
 */
public class CubicEasing extends AbstractEasingFunction {

	/**
	 * Constructor
	 * @param a Coefficient of the cubic term.
	 */
	public CubicEasing(float a) {
		this.params.put("a", a);
	}
	
	@Override
	public float evaluateY(float x) {
		return getParam("a")*x*x*x;
	}

	@Override 
	public String toString() {
		return "Cubic";
	}

	@Override
	public String getFormulaHTML() {
		return "<html>" + getParam("a") + " * x<sup>3</sup></html>";
	}
}