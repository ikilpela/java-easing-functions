package eu.ismokilpelainen.easingfunctions;

import java.awt.geom.GeneralPath;
import java.util.HashMap;
import java.util.Set;

/**
 * Abstract class containing default implementations for most of the methods defined in EasingFunction interface.
 * This class can be extended to create your own functions.
 * 
 * @author Ismo Kilpeläinen
 *
 */
public abstract class AbstractEasingFunction implements EasingFunction {

	/**
	 * HashMap containing parameters for the function
	 */
	protected HashMap<String,Float> params = new HashMap<String,Float>();
	
	@Override
	public GeneralPath getPath(float xmax, int points) {
		return getPath(0,xmax,points);
	}

	@Override
	public GeneralPath getPath(float xmin, float xmax, int points) {
		float step = (xmax-xmin)/points;
		GeneralPath gp = new GeneralPath();
		for (float x=xmin; x <= xmax; x+=step) {
			if (x != xmin) {
				float y = evaluateY(x);
				gp.lineTo(x, y);
			} else {
				gp.moveTo(xmin, evaluateY(x));
			}
		}
		return gp;
	}

	@Override
	public Set<String> getParamKeys() {
		return params.keySet();
	}
	
	@Override
	public float getParam(String key) {
		return params.get(key);
	}
	
	@Override
	public void setParam(String key, float value) {
		params.replace(key, value);
	}
}
