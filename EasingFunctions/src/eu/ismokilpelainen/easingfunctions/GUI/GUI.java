package eu.ismokilpelainen.easingfunctions.GUI;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import eu.ismokilpelainen.easingfunctions.CubicEasing;
import eu.ismokilpelainen.easingfunctions.EasingFunction;
import eu.ismokilpelainen.easingfunctions.LinearEasing;
import eu.ismokilpelainen.easingfunctions.PIDFunction;
import eu.ismokilpelainen.easingfunctions.QuadraticEasing;

import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.Component;
import java.awt.Dimension;

import net.miginfocom.swing.MigLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class GUI extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField xminField;
	private JTextField yminField;
	private JTextField xmaxField;
	private JTextField ymaxField;
	
	private static EasingFunction[] functions = new EasingFunction[] {
			new LinearEasing(1),
			new QuadraticEasing(1),
			new CubicEasing(1),
			new PIDFunction(50,2.4f,200,-200,100f,3f)
	};
	private GraphPanel graphPanel;
	private JComboBox<EasingFunction> comboBox;
	private JPanel paramsPanel;
	
	public GUI() {
		
		JSplitPane splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(new MigLayout("", "[::10][grow][40.00][40.00]", "[14px][28.00px][11.00][][][][][][]"));
		
		JLabel lblGraphType = new JLabel("Easing type");
		panel.add(lblGraphType, "cell 0 0,alignx left,aligny center");
		
		comboBox = new JComboBox<EasingFunction>();
		panel.add(comboBox, "cell 0 1 4 1,growx,aligny top");
		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				EasingFunction f = (EasingFunction)e.getItem();
				graphPanel.setFunction(f);
				paramsPanel.removeAll();
				for (String key : f.getParamKeys()) {
					paramsPanel.add(new JLabel(key));
					JTextField tf = new JTextField();
					tf.setText(String.valueOf(f.getParam(key)));
					tf.setName(key);
					paramsPanel.add(tf);
				}
				paramsPanel.revalidate();
			}
		});
				
		JLabel lblMin = new JLabel("Min");
		panel.add(lblMin, "cell 2 2,alignx center,aligny bottom");
		
		JLabel lblMax = new JLabel("Max");
		panel.add(lblMax, "cell 3 2,alignx center,aligny bottom");
		
		JLabel lblInput = new JLabel("Input (X)");
		lblInput.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblInput, "cell 1 3,alignx trailing");

		xminField = new JTextField();		
		panel.add(xminField, "cell 2 3,growx");
		xminField.setColumns(10);
		xminField.setName("xmin");
		xminField.setText("-100");
				
		xmaxField = new JTextField();
		panel.add(xmaxField, "cell 3 3,growx");
		xmaxField.setColumns(10);
		xmaxField.setName("xmax");
		xmaxField.setText("100");
		
		JLabel lblY = new JLabel("Response (Y)");
		panel.add(lblY, "cell 1 4,alignx trailing");
		
		yminField = new JTextField();
		panel.add(yminField, "cell 2 4,growx");
		yminField.setColumns(10);
		yminField.setName("ymin");
		yminField.setText("0");
		
		ymaxField = new JTextField();
		panel.add(ymaxField, "cell 3 4,growx");
		ymaxField.setColumns(10);
		ymaxField.setName("ymax");
		ymaxField.setText("600");
		
		JButton btnNewButton = new JButton("Redraw");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphPanel.setXmin(Integer.parseInt(xminField.getText()));
				graphPanel.setYmin(Integer.parseInt(yminField.getText()));
				graphPanel.setXmax(Integer.parseInt(xmaxField.getText()));
				graphPanel.setYmax(Integer.parseInt(ymaxField.getText()));
			}
		});
		panel.add(btnNewButton, "cell 2 5 2 1");
		
		JLabel lblParameters = new JLabel("Parameters");
		panel.add(lblParameters, "cell 1 6");
		
		paramsPanel = new JPanel();
		panel.add(paramsPanel, "flowx,cell 1 7 3 1,growx,aligny top");
		paramsPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JButton button = new JButton("Set parameters");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Component c : paramsPanel.getComponents()) {
					if (c instanceof JTextField) {
						JTextField tf = (JTextField)c;
						graphPanel.getFunction().setParam(tf.getName(),Float.parseFloat(tf.getText()));
						graphPanel.repaint();
					}
				}
			}
		});
		panel.add(button, "cell 1 8 3 1,alignx right");
		
		JPanel rightPanel = new JPanel();
		splitPane.setRightComponent(rightPanel);
		rightPanel.setLayout(new BorderLayout(0, 0));
		
		graphPanel = new GraphPanel(new QuadraticEasing(1), -100,100,-100,100);
		rightPanel.add(graphPanel);
		graphPanel.setPreferredSize(new Dimension(400,400));
		addPremadeFunctions();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GUI g = new GUI();
				g.setTitle("Easing function creator");
				g.setLocationRelativeTo(null);
				g.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				g.pack();
				g.setVisible(true);
			}
		});
	}
	
	private void addPremadeFunctions() {
		for (EasingFunction f : functions) {
			comboBox.addItem(f);
		}
	}
}
