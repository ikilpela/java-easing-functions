package eu.ismokilpelainen.easingfunctions;


/**
 * Class representing linear function of form y=a*x.
 * 
 * @author Ismo
 *
 */
public class LinearEasing extends AbstractEasingFunction {

	/**
	 * Constructor. 
	 * @param a Coefficient of the linear function
	 */
	public LinearEasing(float a) {
		this.params.put("a", a);
	}
	
	@Override
	public float evaluateY(float x) {
		return getParam("a")*x;
	}
	
	@Override
	public String toString() {
		return "Linear";
	}

	@Override
	public String getFormulaHTML() {
		return "<html>"+getParam("a")+" * x</html>";
	}
}
