package eu.ismokilpelainen.easingfunctions;

/**
 * Class representing quadratic easing function of form y=a*x<sup>2</sup>.
 * 
 * @author Ismo Kilpeläinen
 */
public class QuadraticEasing extends AbstractEasingFunction {

	/**
	 * Constructor.
	 * @param a Coefficient of the quadratic term.
	 */
	public QuadraticEasing(float a) {
		this.params.put("a", a);
	}
	
	@Override
	public float evaluateY(float x) {
		return getParam("a")*x*x;
	}

	@Override 
	public String toString() {
		return "Quadratic";
	}

	@Override
	public String getFormulaHTML() {
		return "<html>"+ getParam("a") +" * x<sup>2</sup></html>";
	}
}
