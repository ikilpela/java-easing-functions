package eu.ismokilpelainen.easingfunctions.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import eu.ismokilpelainen.easingfunctions.CubicEasing;
import eu.ismokilpelainen.easingfunctions.EasingFunction;
import eu.ismokilpelainen.easingfunctions.EasingSerializer;
import eu.ismokilpelainen.easingfunctions.LinearEasing;
import eu.ismokilpelainen.easingfunctions.QuadraticEasing;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.naming.OperationNotSupportedException;

public class EasingSerializerTest {

	DataOutputStream dataOutputStream;
	DataInputStream dataInputStream;
	File file = new File("test.txt");
	
	@Before
	public void setUp() throws Exception {
		dataOutputStream = new DataOutputStream(new FileOutputStream(file));
		dataInputStream = new DataInputStream(new FileInputStream(file));
	}

	@After
	public void tearDown() throws Exception {
		dataOutputStream.close();
		dataInputStream.close();
	}

	@Test
	public void SentLinearEasing_ShouldBeReceivedAsLinearEasing() throws Exception {
		// Arrange
		float a = 0.1234557f;
		LinearEasing f = new LinearEasing(a);
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertTrue(received instanceof LinearEasing);
	}
	
	@Test
	public void ReceivedLinearEasing_ShouldHaveCorrectParameter() throws Exception {
		// Arrange
		float a = 0.1234557f;
		LinearEasing f = new LinearEasing(a);
		float expected = 0.1234557f;
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertEquals(expected,received.getParam("a"),0.0000001);
	}
	
	@Test
	public void SentQuadraticEasing_ShouldBeReceivedAsQuadraticEasing() throws Exception {
		// Arrange
		float a = 0.1234557f;
		QuadraticEasing f = new QuadraticEasing(a);
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertTrue(received instanceof QuadraticEasing);
	}
	
	@Test
	public void ReceivedQuadraticEasing_ShouldHaveCorrectParameter() throws Exception {
		// Arrange
		float a = 0.1234557f;
		QuadraticEasing f = new QuadraticEasing(a);
		float expected = 0.1234557f;
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertEquals(expected,received.getParam("a"),0.0000001);
	}
	
	
	@Test
	public void SentCubicEasing_ShouldBeReceivedAsCubicEasing() throws Exception {
		// Arrange
		float a = 0.1234557f;
		CubicEasing f = new CubicEasing(a);
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertTrue(received instanceof CubicEasing);
	}
	
	@Test
	public void ReceivedCubicEasing_ShouldHaveCorrectParameter() throws Exception {
		// Arrange
		float a = 0.1234557f;
		CubicEasing f = new CubicEasing(a);
		float expected = 0.1234557f;
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		EasingFunction received = EasingSerializer.receive(dataInputStream);
		
		// Assert
		assertEquals(expected,received.getParam("a"),0.0000001);
	}
	
	@Test
	public void ReceiveEasing_WithIncorrectHeaderByte_ShouldThrowCorrectIOException() {
		// Arrange
		byte b = 12;
		String expectedMessage = "Invalid header byte";
		
		try {
			// Act
			dataOutputStream.write(b);
			EasingSerializer.receive(dataInputStream);
			
			// Assert
			fail();
			
		} catch (IOException e) {
			assertEquals(expectedMessage,e.getMessage());
		}
	}
	@Test(expected=OperationNotSupportedException.class) 
	public void SendEasing_WithUnsupportedName_ShouldThrowException() throws Exception {
		// Arrange
		EasingFunction f = new LinearEasing(1) {
			@Override
			public String toString() {
				return "Blaa";
			}
		};
		
		// Act
		EasingSerializer.send(dataOutputStream, f);
		
		// Assert
		fail();
	}
}
