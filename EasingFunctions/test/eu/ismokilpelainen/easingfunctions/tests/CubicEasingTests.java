package eu.ismokilpelainen.easingfunctions.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import eu.ismokilpelainen.easingfunctions.CubicEasing;
import org.junit.runners.JUnit4;
import org.junit.runners.Parameterized;


@RunWith(Enclosed.class)
public class CubicEasingTests {

	@RunWith(Parameterized.class)
	public static class ParameterizedCubicEasingTests {
		
		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
				{-7880599,-199},{-1000000,-100},{-117649,-49},{-1,-1},{0,0},{1,1},{132651,51},{1030301,101},{8120601,201}
			});
		}
		
		private float delta = 0.000001f;
		private float input;
		private float expectedOutput;
	
		public ParameterizedCubicEasingTests(float expected, float input) {
			this.input = input;
			this.expectedOutput = expected;
		}
		
		@Test
		public void evaluateY_ShouldReturnCorrectValues() {
			// Arrange
			CubicEasing f = new CubicEasing(1);
			
			// Act
			float output = f.evaluateY(input);
			
			//Assert
			assertEquals(expectedOutput,output,delta);
		}
	}
	
	@RunWith(JUnit4.class)
	public static class NonParameterizedTests {
	
		@Test
		public void getFormulaHTML_ShouldReturnCorrectString() {
			// Arrange
			CubicEasing f = new CubicEasing(3);
			String expected = "<html>"+ 3f +" * x<sup>3</sup></html>";
			
			// Act
			String output = f.getFormulaHTML();
			
			// Assert
			assertEquals(expected,output);
		}
	}
}
