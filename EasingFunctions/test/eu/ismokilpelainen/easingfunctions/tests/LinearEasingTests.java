package eu.ismokilpelainen.easingfunctions.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import eu.ismokilpelainen.easingfunctions.LinearEasing;

import org.junit.runners.JUnit4;
import org.junit.runners.Parameterized;


@RunWith(Enclosed.class)
public class LinearEasingTests {

	@RunWith(Parameterized.class)
	public static class ParameterizedLinearEasingTests {
		
		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
				{-199,-199},{-100,-100},{-49,-49},{-1,-1},{0,0},{1,1},{51,51},{101,101},{201,201}
			});
		}
		
		private float delta = 0.000001f;
		private float input;
		private float expectedOutput;
	
		public ParameterizedLinearEasingTests(float expected, float input) {
			this.input = input;
			this.expectedOutput = expected;
		}
		
		@Test
		public void evaluateY_ShouldReturnCorrectValues() {
			// Arrange
			LinearEasing f = new LinearEasing(1);
			
			// Act
			float output = f.evaluateY(input);
			
			//Assert
			assertEquals(expectedOutput,output,delta);
		}
	}
	
	@RunWith(JUnit4.class)
	public static class NonParameterizedTests {
	
		@Test
		public void getFormulaHTML_ShouldReturnCorrectString() {
			// Arrange
			LinearEasing f = new LinearEasing(3);
			String expected = "<html>"+ 3f +" * x</html>";
			
			// Act
			String output = f.getFormulaHTML();
			
			// Assert
			assertEquals(expected,output);
		}
	}
}
