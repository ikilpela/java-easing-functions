package eu.ismokilpelainen.easingfunctions.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;
import eu.ismokilpelainen.easingfunctions.QuadraticEasing;

@RunWith(Enclosed.class)
public class QuadraticEasingTests {

	@RunWith(Parameterized.class)
	public static class ParameterizedTests {
		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
				{39601,-199},{10000,-100},{2401,-49},{1,-1},{0,0},{1,1},{2601,51},{10201,101},{40401,201}
			});
		}
		
		private float delta = 0.000001f;
		private float input;
		private float expectedOutput;
	
		public ParameterizedTests(float expected, float input) {
			this.input = input;
			this.expectedOutput = expected;
		}
		
		@Test
		public void evaluateY_ShouldReturnCorrectValues() {
			// Arrange
			QuadraticEasing f = new QuadraticEasing(1);
			
			// Act
			float output = f.evaluateY(input);
			
			//Assert
			assertEquals(expectedOutput,output,delta);
		}
	}
	
	public static class NonParameterizedTests {
	
		@Test
		public void getFormulaHTML_ShouldReturnCorrectString() {
			// Arrange
			QuadraticEasing f = new QuadraticEasing(3);
			String expected = "<html>"+ 3f +" * x<sup>2</sup></html>";
			
			// Act
			String output = f.getFormulaHTML();
			
			// Assert
			assertEquals(expected,output);
		}
	}
}
